# -*- coding: utf-8 -*-
"""
Created on Tue May 28 00:50:49 2019

@author: David
"""
from person import Person
class Superhero(Person):
    def __init__(self, name, age, secret_identity):
        self.name=name
        self.age=age
        self.secret_identity=secret_identity
    def use_super_sight(self):
        print(self.name," (aka ",self.secret_identity,") uses super sight",sep='')
    def walk(self, use_power=False):
        if(use_power==False):
            return Person.walk(self)
        if(use_power==1):
            print(self.name," (aka ",self.secret_identity,") uses extra-speed running",sep='')
        elif(use_power==0):
            return Person.walk(self)