# -*- coding: utf-8 -*-
"""
Created on Tue May 28 16:24:53 2019

@author: David
"""

class Vehicle:
    def __init__(self,model_year,kilometers):
        self.model_year=model_year
        self.kilometers=kilometers
    def drive(self, new_kilometers):
        self.kilometers=self.kilometers+new_kilometers
        print("After ",new_kilometers," kilometers, odometer reading is ", self.kilometers, sep='')
     