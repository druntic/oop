# -*- coding: utf-8 -*-
"""
Created on Tue May 28 16:24:48 2019

@author: David
"""

from vehicle import Vehicle
from car import Car
from motorcycle import Motorcycle

v=Vehicle(2018,0)
v.drive(47)
v.drive(35)
c=Car(2018,0,9001)
c.drive(0)
m=Motorcycle(2018,0)
m.ride_on_the_rear_wheel()