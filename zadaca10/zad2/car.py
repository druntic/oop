# -*- coding: utf-8 -*-
"""
Created on Tue May 28 16:24:52 2019

@author: David
"""
from vehicle import Vehicle
class Car(Vehicle):
    def __init__(self,model_year,kilometers,number_of_doors ):
        self.model_year=model_year
        self.kilometers=kilometers
        self.number_of_doors=number_of_doors
        