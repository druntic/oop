# -*- coding: utf-8 -*-
"""
Created on Tue May 28 16:24:48 2019

@author: David
"""

from vehicle import Vehicle
class Motorcycle(Vehicle):
    def ride_on_the_rear_wheel(self):
        print("Motorcycle rides on the rear wheel")