# -*- coding: utf-8 -*-
"""
Created on Tue May 28 00:52:07 2019

@author: David
"""

class Person:
    def __init__(self,name,age):
        self.name=name
        self.age=age
    def walk(self):
        print(self.name, " (",self.age,") walks",sep='')
    def say(self, message):
        print(self.name,"says: ",message)
