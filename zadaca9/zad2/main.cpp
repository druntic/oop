/******************************************************************************

                              Online C++ Compiler.
               Code, Compile, Run and Debug C++ program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include "fun.h"


int main()
{
    //cout<<"Hello World";
    StaticContainer<int,2> l1;
    StaticContainer<int,2> l2;
    l1[0]=2;
    l1[1]=3;
    l2[0]=4;
    l2[1]=5;
    //l2.print(cout);
    DynamicContainer<int,2> k;
    k[1]=5;
    k[0]=8;
    //k.print(cout);
    Container<int,2>* c[3]={(Container<int, 2>*) &l1,(Container<int, 2>*) &l2, (Container<int, 2>*) &k};
    int sum=0;
    for(auto& i:c)
        sum+=i->sum();
    cout<<sum;





    return 0;
}
