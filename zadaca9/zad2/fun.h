//
// Created by David on 5/21/2019.
//

#ifndef DZ10ZAD2_FUN_H
#define DZ10ZAD2_FUN_H
#include <iostream>
#include <list>
#include <vector>
#include <initializer_list>
using namespace std;

template <typename T, size_t S>
class Container {
protected:
    // kod nasljeđene klase vraca
    // pokazivač na prvi element polja
    virtual T* first() = 0;
    // kod nasljeđene klase vraća
    // pokazivač na konstantni prvi element polja
    virtual const T* first() const = 0;
public:
    Container() = default;
    virtual ~Container() = default;
    // vrati pokazivač na prvi element polja
    T* begin(){first();};
    // vrati pokazivač na zadnji element polja
    T* end(){first()+S-1;};
    // vrati pokazivač na konstantni prvi element polja
    const T* begin() const{first();};
    // vrati pokazivač na konstantni zadnji element polja
    const T* end() const{first()+S-1;};
    // preopterećeni operator dohvaćanja
    T& operator[](int i){return *(first()+i);};
    // konstantni preopterećeni operator dohvaćanja
    const T& operator[](int i) const{return *(first()+i);};
    virtual void print(ostream& os) const = 0;
    virtual int sum()=0;
    friend int main();
};
template <typename T, size_t S>
class StaticContainer: private Container<T,S>
{
protected:
    T list[S];
    T* first()override{return &list[0];};
    const T* first()const{return &list[0];};
public:
    StaticContainer()=default;
    StaticContainer(initializer_list<T> l)
    {
        int count=0;
        for(auto& i:l)
        {list[count]=i;
            count++;}

    };
    ~StaticContainer()=default;
    void print(ostream& os)const override
    {
        for(auto& i:list)
            os<<i<<" ";
    }
    int sum()
    {
        int sum=0;
        for(int i=0;i<S;i++)
            sum+=*(list+i);
        return sum;
    }
    //void set(int place, int key){this[place]=key;}
    friend int main();

};
template <typename T, size_t S>
class DynamicContainer: private Container<T,S>{
protected:
    T* list;
    const T* first()const override{return list;}
    T* first()override{return list;}
public:

    DynamicContainer(){ list=new T[S];
        for(int i=0;i<S;i++)
            *(list+i)=0;
    }
    DynamicContainer(vector<T> l){list=new T[S];
        int count=0;
        for(auto& i:l)
        {list[count]=i;count++;}
    }
    ~DynamicContainer(){delete[] list;}
    void print(ostream& os)const override {
        for(int i=0;i<S;i++)
            os<<*(list+i)<<" ";


    };
    int sum()
    {
        int sum=0;
        for(int i=0;i<S;i++)
            sum+=*(list+i);
        return sum;
    }
    //T& operator[](int i){return *(first()+i);};
    friend int main();
};

#endif //DZ10ZAD2_FUN_H
