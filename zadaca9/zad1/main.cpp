#include <iostream>

using namespace std;


class Oblik{
protected:
    double width,height,radius;
public:
    Oblik(double a, double b){width=a;height=b;}
    Oblik(double r){radius=r;}
    virtual double area()const{};
};

class Rectangle: public Oblik {
private:
    //double width, height;
public:

    Rectangle(double width, double height):Oblik(width,height){};
    double area()const override {return width*height;};
};
class Circle: public Oblik {
private:
    //double radius;
public:

    Circle(double radius):Oblik(radius){};
    double area()const override {return 3.14*3.14*radius;};
};




int main() {

    Rectangle r1(5.5,7.2);
    Rectangle r2(0.2,1.5);
    Rectangle r3(8.9,9.8);
    Circle c1(1.5);
    Circle c2(2.6);
    Circle c3(2.7);
    Oblik* o[6]={};
    o[0]=&r1;
    o[1]=&r2;
    o[2]=&r3;
    o[3]=&c1;
    o[4]=&c2;
    o[5]=&c3;
    double sum=0;
   for(auto i:o)
       sum+=i->area();
   cout<<sum;

    return 0;
}