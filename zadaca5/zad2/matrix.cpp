//
// Created by David on 3/31/2019.
//
#include <iostream>
#include "matrix.h"
using namespace std;
/*
* pokazivač na dimanički alocirano polje duljine M čiji su elementi
* pokazivači na dinamički alocirana polja duljine M (retci u matrici)
*/


//SquareMatrix::SquareMatrix() = delete;
// inicijaliziraj sve ćelije na 0.0
SquareMatrix::SquareMatrix(unsigned int M)
{
    this->M=M;
    container=new double*[M];
    for(int i=0;i<M;i++)
    {
        container[i]=new double[M];
        for(int j=0;j<M;j++)
        {
            container[i][j]=0;
        }
    }
};
SquareMatrix::SquareMatrix(const SquareMatrix& c)
{
    this->M=c.M;
    container=new double*[M];
    for(int i=0;i<M;i++)
    {
        container[i]=new double[M];
        for(int j=0;j<M;j++)
        {
            container[i][j]=c.container[i][j];
        }
    }
};
SquareMatrix::~SquareMatrix()
{
    for(int i = 0; i < M; i++)
        delete [] container[i];
    delete [] container;
};

void SquareMatrix::print()
{

    for(int i=0;i<M;i++)
    {

        for(int j=0;j<M;j++)
        {
            cout<<container[i][j]<<" ";
        }
        cout<<endl;
    }
};
double* SquareMatrix::operator[](unsigned int i){return container[i];};
const double* SquareMatrix::operator[](unsigned int i) const{return container[i];};
SquareMatrix& SquareMatrix::operator=(const SquareMatrix& c)
        {
            M=c.M;
            container=c.container;
            return *this;
        };
SquareMatrix& SquareMatrix::operator+=(const SquareMatrix& c)
        {
    for(int i=0;i<M;i++)
    {
        for(int j=0;j<M;j++)
        {
            container[i][j]=container[i][j]+c.container[i][j];
        }
    }
    return *this;
        };

SquareMatrix& SquareMatrix::operator-=(const SquareMatrix& c)
{
    for(int i=0;i<M;i++)
    {
        for(int j=0;j<M;j++)
        {
            container[i][j]=container[i][j]-c.container[i][j];
        }
    }
    return *this;
};
SquareMatrix& SquareMatrix::operator*=(const SquareMatrix& c)
        {
    SquareMatrix a(M);
            for (int i = 0; i < M; i++)
            {
                for (int j = 0; j < M; j++)
                {
                    a[i][j]=0;
                    for (int k= 0; k < M; k++)
                    {a[i][j]+= container[i][k]*c.container[k][j];

                    }
                }
            }

            for(int i=0;i<M;i++)
            {
                for(int j=0;j<M;j++)
                {
                    container[i][j]=a[i][j];
                }
            }
            delete &a;

            return *this;
        }


// postavi sve ćelije na isti skalar
SquareMatrix& SquareMatrix::operator=(double x)
        {
            for(int i=0;i<M;i++)
            {
                for(int j=0;j<M;j++)
                {
                    container[i][j]=x;
                }
            }
            return *this;
        };
//svim ćelijama nadodaj isti skalar
SquareMatrix& SquareMatrix::operator+=(double x)
{
        for(int i=0;i<M;i++)
            {
                for(int j=0;j<M;j++)
                {
                    container[i][j]=container[i][j]+x;
                }
            }
            return *this;
};
// od svih ćelija oduzmi isti skalar
SquareMatrix& SquareMatrix::operator-=(double x)
{
    for(int i=0;i<M;i++)
    {
        for(int j=0;j<M;j++)
        {
            container[i][j]=container[i][j]-x;
        }
    }
    return *this;
};
// svaku ćeliju pomnoži sa skalarom
SquareMatrix& SquareMatrix::operator*=(double x)
{
    for(int i=0;i<M;i++)
    {
        for(int j=0;j<M;j++)
        {
            container[i][j]=container[i][j]*x;
        }
    }
    return *this;
};
// svaku ćeliju podijeli sa skalarom
SquareMatrix& SquareMatrix::operator/=(double x)
{
    for(int i=0;i<M;i++)
    {
        for(int j=0;j<M;j++)
        {
            container[i][j]=container[i][j]/x;
        }
    }
    return *this;
};
SquareMatrix SquareMatrix::operator+(const SquareMatrix& c) const
{
    SquareMatrix a(M);
    for(int i=0;i<M;i++)
    {
        for(int j=0;j<M;j++)
        {
            a[i][j]=container[i][j]+c.container[i][j];
        }
    }
    return a;
};
SquareMatrix SquareMatrix::operator-(const SquareMatrix& c) const
{
    SquareMatrix a(M);
    for(int i=0;i<M;i++)
    {
        for(int j=0;j<M;j++)
        {
            a[i][j]=container[i][j]-c.container[i][j];
        }
    }
    return a;
};
SquareMatrix SquareMatrix::operator*(const SquareMatrix& c) const

    {
        SquareMatrix a(M);
        for (int i = 0; i < M; i++)
        {
            for (int j = 0; j < M; j++)
            {
                a[i][j]=0;
                for (int k= 0; k < M; k++)
                {a[i][j]+= container[i][k]*c.container[k][j];

                }
            }
        }

        return a;
    };
SquareMatrix SquareMatrix::operator+(double x) const
{
    SquareMatrix a(M);
    for (int i = 0; i < M; i++)
    {
        for (int j = 0; j < M; j++)
        {
            a[i][j]+= container[i][j]+x;
        }
    }

    return a;
};
SquareMatrix SquareMatrix::operator-(double x) const
{
    SquareMatrix a(M);
    for (int i = 0; i < M; i++)
    {
        for (int j = 0; j < M; j++)
        {
            a[i][j]+= container[i][j]-x;
        }
    }

    return a;
};
SquareMatrix SquareMatrix::operator*(double x) const
{
    SquareMatrix a(M);
    for (int i = 0; i < M; i++)
    {
        for (int j = 0; j < M; j++)
        {
            a[i][j]+= container[i][j]*x;
        }
    }

    return a;
};
SquareMatrix SquareMatrix::operator/(double x) const
{
    SquareMatrix a(M);
    for (int i = 0; i < M; i++)
    {
        for (int j = 0; j < M; j++)
        {
            a[i][j]+= container[i][j]/x;
        }
    }

    return a;
};
bool SquareMatrix::operator==(const SquareMatrix& c) const
{
    for (int i = 0; i < M; i++)
    {
        for (int j = 0; j < M; j++)
        {
            if(container[i][j]!=c.container[i][j]){return false;}
        }
    }
    return true;
};
bool SquareMatrix::operator!=(const SquareMatrix& c) const
{
    for (int i = 0; i < M; i++)
    {
        for (int j = 0; j < M; j++)
        {
            if(container[i][j]==c.container[i][j]){return false;}
        }
    }
    return true;
};





