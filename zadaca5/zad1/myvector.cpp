//
// Created by David on 3/29/2019.
//

#include "myvector.h"
#include <iostream>
#include <algorithm>

using namespace std;


MyVector::MyVector(){P= new int[0];_size=0;_capacity=0;};
MyVector::MyVector(int n){P= new int[n];_size=0;_capacity=n;};
MyVector::MyVector(const MyVector& v)//kopira vektor
{
    _capacity=v._capacity;
    P= new int [_capacity];
    _size=v._size;
    for (int i=0; i<v._size;i++)
    {
        P[i] = v.P[i];
    }

};
MyVector::~MyVector(){delete [] P;};





void MyVector::pushBack(int x)
{
    if(_capacity==0){_capacity=1;}
    if(_size==_capacity)
    {
        _capacity=2*_capacity;
        int* P1= new int[_capacity];
        for(int i=0;i<_size;i++)
        {
            P1[i]=P[i];
        }
        delete [] P;
        P=P1;
    }
    P[_size++]=x;
    //cout<<"size: "<<_size<<endl;


};

int MyVector::getSize() const{return _size;};
int MyVector::getCapacity() const{return _capacity;}
void MyVector::print()
{
    for(int i=0;i<_size;i++)
    {
        cout<<P[i]<<" ";

    }
    cout<<endl;
};
bool MyVector::empty()
{
    if(_size==0)
    {
        return true;
    }
    else
    {
        return false;
    }
};
bool MyVector::full()
{
    if(_size==_capacity){return true;}
    else{return false;}
};

int& MyVector::at( int pos){return P[pos];};
int& MyVector::front()
{

    return P[0];


};
int& MyVector::back()
{
    return P[_size];
};

//operator pridruživanja
MyVector& MyVector::operator=(const MyVector& v)
{
    // delete current mojVector content
    if (P!= nullptr) delete [] P;
    _size = 0;
    _capacity = 0;
    // iskopiraj v u trenutni vektor
    P = new int[v._capacity];
    _capacity = v._capacity;
    _size = v._size;
    for (int i=0;i<_size;i++)
        P[i] = v.P[i];
};
/*
Povećaj element na itom mjestu za vrijednost
koja se nalazi na itom mjestu vektora
prosljeđenog po referenci
*/
MyVector& MyVector::operator+=(const MyVector& v)
        {
            for (int i=0;i<_size;i++)
                P[i] = P[i]+v.P[i];
            return *this;
        };
//zbroj vektora
MyVector MyVector::operator+(const MyVector& v) const
{
    int m=min(v.getSize(), getSize());
    int M=max(v.getSize(), getSize());
    MyVector* V=new MyVector[M];
    for(int i=0;i<m;i++){
        V->pushBack(P[i]+v.P[i]);
    }
return *V;
};
//skalarni produkt vektora
int MyVector::operator*(const MyVector& v) const
{
    int prod=0;
    int m=min(v.getSize(), getSize());
    for(int i=0;i<m;i++)
    {
        prod=prod+(P[i]*v.P[i]);
    }
    return prod;

};
//provjera jesu li dva vektora jednaki po elementima
bool MyVector::operator==(const MyVector& v) const
{
    int m=min(v.getSize(), getSize());
    for(int i=0;i<m;i++)
    {
      if(P[i]!=v.P[i]){return false;}
      return true;
    }
};
//provjera jesu li dva vektora različiti po elementima
bool MyVector::operator!=(const MyVector& v) const
{
    int m=min(v.getSize(), getSize());
    for(int i=0;i<m;i++)
    {
        if(P[i]==v.P[i]){return false;}
        return true;
    }
};
 int& MyVector::operator[](int i)
{
    return P[i];
}
const int& MyVector::operator[](int i)const
{
    return P[i];
}


int* MyVector::begin() const
{
return &P[0];
};
int* MyVector::end() const
{
    return &P[_size];
};