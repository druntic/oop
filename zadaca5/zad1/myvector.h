//
// Created by David on 3/29/2019.
//

#ifndef DZ5ZAD1NOVI_MYVECTOR_H
#define DZ5ZAD1NOVI_MYVECTOR_H
class MyVector {
protected:
    unsigned int _size, _capacity;
    int* P;
public:
    MyVector();
    MyVector(const MyVector&);
    MyVector(int);
    ~MyVector();
    void pushBack(int);

    int getCapacity() const;
    int getSize() const;
    bool empty();
    bool full();
    int& at(int pos);
    int& front();
    int& back();
    void print();

//operator pridruživanja
    MyVector& operator=(const MyVector&);
    /*
    Povećaj element na itom mjestu za vrijednost
    koja se nalazi na itom mjestu vektora
    prosljeđenog po referenci
    */
    MyVector& operator+=(const MyVector&);
    //zbroj vektora
    MyVector operator+(const MyVector&) const;
    //skalarni produkt vektora
    int operator*(const MyVector&) const;
    //provjera jesu li dva vektora jednaki po elementima
    bool operator==(const MyVector&) const;
    //provjera jesu li dva vektora različiti po elementima
    bool operator!=(const MyVector&) const;
    //operatori dohvaćanja
    const int& operator[](int) const;
    int& operator[](int);
    /*
    početak i kraj vektora
    (obratite pažnju što treba vraćati end)
    */
    int* begin() const;
    int* end() const;

};
#endif //DZ5ZAD1NOVI_MYVECTOR_H
