/******************************************************************************

                              Online C++ Compiler.
               Code, Compile, Run and Debug C++ program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <iostream>
#include <vector>

using namespace std;

class DynamicStack {
protected:
 vector<int> container;
public:
 DynamicStack(){container.resize(0);};
 DynamicStack(int n){container.resize(n);};
 DynamicStack(const DynamicStack& S)
 {
     for(int i=0;i<S.container.size();i++)
     {
         container.push_back(S.container[i]);
    
     }
     
 };
 
 
 bool empty() const{container.empty();};
 
 
 void push(int x){container.push_back(x);};
 
 int pop(){container.pop_back();};
 int size(){return container.size();};
 
 void print()
 {
     for(int i=0;i<container.size();i++)
     {
         cout<<container[i]<<" ";
     }
     cout<<endl;
 };
};




int main()
{

DynamicStack *x1= new DynamicStack();
for(int i=1;i<=10;i++)
{
    x1->push(i);
}
x1->print();

DynamicStack *x2= new DynamicStack(*x1);
for(int i=x2->size();i>0;i--)
{
    x2->pop();
    x2->print();
}





    return 0;
}


