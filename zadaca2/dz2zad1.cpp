/******************************************************************************

                              Online C++ Compiler.
               Code, Compile, Run and Debug C++ program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <iostream>

using namespace std;
class MyVector {
protected:
 unsigned int _size, _capacity;
 int* P;
public:
 MyVector(){P= new int[0];_size=0;_capacity=0;};
 MyVector(int n){P= new int[n];_size=0;_capacity=n;};
 MyVector(const MyVector& v)//kopira vektor
 {
    _capacity=v._capacity;     
    P= new int [_capacity];
    _size=v._size;
    for (int i=0; i<v._size;i++)
    {
        P[i] = v.P[i];
    }

 };
 ~MyVector(){delete [] P;};
 
 
 
 
 
 void pushBack(int x)
 {
     if(_capacity==0){_capacity=1;}
     if(_size==_capacity)
     {
         _capacity=2*_capacity;
         int* P1= new int[_capacity];
         for(int i=0;i<_size;i++)
         {
             P1[i]=P[i];
         }
         delete [] P;
         P=P1;
     }
     P[_size++]=x;
     //cout<<"size: "<<_size<<endl;
     
     
 };
int popBack()
{
    int x=P[_size];
    --_size;
    if(_capacity==2*_size)
     {
         _capacity=0.5*_capacity;
         int* P1= new int[_capacity];
         for(int i=0;i<_size;i++)
         {
             P1[i]=P[i];
         }
         delete [] P;
         P=P1;
     }
     return x;
    
    
};
 unsigned int getSize() const{return _size;};
 unsigned int getCapacity() const{return _capacity;}
 void print()
 {
     for(int i=0;i<_size;i++)
     {
         cout<<P[i]<<" ";
         
     }
     cout<<endl;
 };
 bool empty() const
 {
     if(_size==0)
     {
         return true;
     }
     else
     {
         return false;
     }
 };
 bool full() const
 {
    if(_size==_capacity){return true;}
    else{return false;}
 };

 int& at(unsigned int pos){return P[pos];};
 int& front()
 {

     return P[0];

     
 };
 int& back()
 {
     return P[_size];
 };
};






int main()
{
int n=10;
    MyVector *v1= new MyVector();
    for(int i=1;i<=n;i++)
    {
        v1->pushBack(i);
    }
    v1->print();
    MyVector *v2= new MyVector(*v1);
    while(v2->getSize()!=0){v2->popBack();v2->print();}
    delete [] v1;


}



