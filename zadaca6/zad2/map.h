//
// Created by David on 7.4.2019..
//

#ifndef DZ6ZAD2_MAP_H
#define DZ6ZAD2_MAP_H

#include <iostream>
#include <utility>
#include <functional>
#include <vector>
using namespace std;


template <typename K, typename V>
class Map {
private:
 vector<pair<K, V>> container{};
public:


 Map()= default;
 Map(const Map<K, V>& m){
     for(auto x:m.container){container.push_back(x);}
 };
 ~Map()= default;
 V& operator[](const K& k)
 {
     if(container.size()==0)
     {
         container.push_back((make_pair(k, V{})));
         //cout<<"size:"<<container.size()<<endl;
         return container.back().second;
     }
     for(auto& x:container)
     {
         if(x.first==k){return x.second;}

         else{
                container.push_back((make_pair(k, V{})));
               // cout<<"size:"<<container.size()<<endl;
         return container.back().second;
         }
     }
 };
 pair<K, V>* begin(){return &container.front();};
 pair<K, V>* end(){return &container.back();};
void print() const
 {
   for(auto& x:container){cout<<x.first<<": "<<x.second<<endl;}
 };

};


#endif //DZ6ZAD2_MAP_H
