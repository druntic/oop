/******************************************************************************

                              Online C++ Compiler.
               Code, Compile, Run and Debug C++ program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include "fun.h"

int main()
{

    Credentials a("Korisnik1", "korisnik1@gmail", 23);
    Credentials b("Korisnik2", "korisnik2@gmail", 24);
    Credentials c("Korisnik3", "korisnik3@gmail", 25);
    Credentials d("Korisnik4", "korisnik4@gmail", 26);
    Credentials e("Korisnik5", "korisnik5@gmail", 27);
    Queue<Credentials, 10> q;
    q.enqueue(a);
    q.enqueue(b);
    q.enqueue(c);
    q.enqueue(d);
    q.enqueue(e);
    q.print();
    q.dequeue();
    q.print();
    Credentials f("Korisnik6", "korisnik6@gmail", 28);
    q.enqueue(f);
    q.print();


    return 0;
}
