//
// Created by David on 8.4.2019..
//

#ifndef DZ6ZAD1_FUN_H
#define DZ6ZAD1_FUN_H
#include <iostream>

using namespace std;

template <typename T, size_t S>
class Queue {
public:
    T* head{};
    T* tail{};
    T container[S + 1];
    Queue(){};
    Queue(const Queue& q)
    {
        auto i=q.head;
        while(i!=q.tail)
        {
            enqueue(*i);
            i++;
        }
        enqueue(*i);
    };
    ~Queue()= default;
    bool empty() const{return tail==nullptr;};
    bool full() const{return tail==&container[S+1];};
    template <typename T1>
    void enqueue(T1 x)
    {
        if(full()){cout<<"full";}
        if(empty()){container[0]=x;head=&container[0];tail=&container[0];


        }
        else
        {
            T1* i=&container[0];
            int j=1;
            while(i!=tail)
            {
                i++;
                j++;
            }
            container[j]=x;
            tail=&container[j];

        }

    };
    T dequeue()
    {
        T* x=head;
        head++;
        return *x;
    };


    template <typename T2 ,typename... Ts>
    void enqueue(T2 x,Ts... ts)
    {
        enqueue(x);
        return enqueue(ts...);
    };



    void print()const
    {
        cout<<"head at: "<<head<<" tail at: "<<tail<<endl;

        auto i=head;
        while(i!=tail)
        {
            cout<<*i;
            i++;
        }
        cout<<*i<<endl;
    }
};
class Credentials{
public:
    string username;
    string email;
    unsigned int age;

    Credentials(){}
    Credentials(string username, string email, unsigned int age): username(username), email(email), age(age) {}
};

ostream& operator<<(ostream& os, const Credentials &C){
    os<<"Username: " << C.username << " Email: " << C.email << " Age: " << C.age << endl;
    return os;
}


#endif //DZ6ZAD1_FUN_H
