/******************************************************************************

                              Online C++ Compiler.
               Code, Compile, Run and Debug C++ program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <iostream>
#include <cmath>
#include<vector>
#include<algorithm>

using namespace std;

struct PositionVector {
    
double x, y;
PositionVector(){x=0;y=0;};

PositionVector(double a, double b)
{
 x=a;
 y=b;
};

PositionVector(const PositionVector& v1)
{
   x=v1.x;
   y=v1.y;
};

double norm() const
{
    return sqrt(x*x+y*y);
};

void print() const
{
    cout<<"koordinate: "<<x<<","<<y<<endl;
    cout<<"norma: "<<norm()<<endl;
};

    
};

bool sort1(PositionVector v1, PositionVector v2)
{
   if(v1.norm()>v2.norm())
   {return true;} 
   else
   {
return false;
   }
}    


int main()
{

PositionVector v[5];
v[0]=PositionVector (2.5, 3.6);
v[1]=PositionVector (5.5, 3.6);
v[2]=PositionVector (4.4, 4.4);
v[3]=PositionVector (10.0, 0.1);
v[4]=PositionVector (0.0, 0.0);
for(PositionVector i:v)
    i.print();
    
sort(v,v+5,sort1);
for(PositionVector i:v)
    i.print();
}

