/******************************************************************************

                              Online C++ Compiler.
               Code, Compile, Run and Debug C++ program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <iostream>
#include <cmath>
using namespace std;

class Rectangle;
class Point {
protected:
 double x, y;
public:
 // pretpostavljeni konstruktor
 Point()
 {
Point(0,0);
 };
 // preoptereceni konstruktor
 Point(double a, double b)
 {
     setx(a);
     sety(b);
 };
 //getter-i
 double getx() const{return x;} ;
 double gety()const{return y;} ;

 //setter-i
 void setx(double a)
 {
     x=a;
 };
 void sety(double b)
 {
     y=b;
 };
 // printaj tocku
 void print()const{cout<<getx()<<" "<<gety()<<endl;} ;
 // je li pravokutnik unutar tocke
bool isInside(Rectangle r) const;

};


class Rectangle {
protected:
 Point A, B, C, D;
public:
 // preoptereceni konstruktor
 Rectangle(Point A, Point B, Point C)
 {
    this->A=A;
    this->B=B;
    this->C=C;
    Point D(A.getx(),C.gety());
    this->D=D;
    
    
 };
 
 Point getA()const{return A;};
 Point getB()const{return B;};
 Point getC()const{return C;};
 Point getD()const{return D;};
 
 
 double distance(Point X, Point Y)
const {
    return sqrt((X.getx()-Y.getx())*(X.getx()-Y.getx())+(X.gety()-Y.gety())*(X.gety()-Y.gety())); 
 };

// Point getD() const;
 // povrsina
 double area() const {return distance(A,B)*distance(B,C);};
 // opseg
 double perimeter() const {return 2*distance(A,B)+2*distance(B,C);}
 // translatiraj za tocku
 void translate(Point X)
 {
     A.setx(A.getx()+X.getx());
     A.sety(A.gety()+X.gety());
     B.setx(B.getx()+X.getx());
     B.sety(B.gety()+X.gety());
     C.setx(C.getx()+X.getx());
     C.sety(C.gety()+X.gety());
     D.setx(D.getx()+X.getx());
     D.sety(D.gety()+X.gety());     

 };
 // rotiraj oko ishodista za kut tipa double u radijanima
void rotate(double phi)// T(Z*cosa,Z*sina)
 {
    Point zero(0,0);
    double A1=distance(A,zero);double Ax=A.getx();double Ay=A.gety();
    double B1=distance(B,zero);double Bx=B.getx();double By=B.gety();
    double C1=distance(C,zero);double Cx=C.getx();double Cy=C.gety();
    double D1=distance(D,zero);double Dx=D.getx();double Dy=D.gety();
    double alpha=acos(Ax/A1)+phi;double alpha1=asin(Ay/A1)+phi;
    double beta=acos(Bx/B1)+phi;double beta1=asin(By/B1)+phi;
    double gamma=acos(Cx/C1)+phi;double gamma1=asin(Cy/C1)+phi;
    double delta=acos(Dx/D1)+phi;double delta1=asin(Dy/D1)+phi;
    A.sety(A1*sin(alpha1));A.setx(A1*cos(alpha));
    B.sety(B1*sin(beta1));B.setx(B1*cos(beta));
    C.sety(C1*sin(gamma1));C.setx(C1*cos(gamma)); 
    D.sety(D1*sin(delta1));D.setx(D1*cos(delta));
        
 };

 
 // ispisi
 void print() const
 {
     A.print();
     B.print();
     C.print();
     D.print();
 };
};
 bool Point::isInside(Rectangle r)const
 {
     if(r.getA().getx()<=x<=r.getC().getx() and r.getA().gety()<=y<=r.getC().gety()){return true;}
     else return false;
 }; 




int main()
{
    Point A(1,2);
    Point B(2,4);
    Point C(4,5);
    Rectangle R(A,B,C);
    cout<<R.distance(A,B)<<endl;
    cout<<R.distance(B,C)<<endl;
    cout<<R.area()<<endl;
    cout<<A.isInside(R);
    R.translate(C);
    R.rotate(3.14);
    

    return 0;
}


