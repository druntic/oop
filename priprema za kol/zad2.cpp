#include <iostream>
#include <cstdlib>
#include <math.h>
using namespace std;

// grid size
float N = 5;
float M = 4;
class Robot
{
protected:
 int _x;
 int _y;
public:
 Robot(int x, int y): _x(x), _y(y){}
 void Left()
 {
     if(_y==0){_y=N-1;}
     else{_y--;}
 };
 void Right()
 {
     if(_y==N-1){_y=0;}
     else{_y++;}
 };
 void Up()
 {
     if(_x==0){_x=M-1;}
     else{_x--;}
 };
 void Down()
 {
     if(_x==M-1){_x=0;}
     else{_x++;}
 };
 void PrintMesh() const
 {
     for(int i=0;i<M;i++)
     {   for(int j=0;j<N;j++)
        {
            if(i==_x & j==_y)
            {cout<<"X";}
            else{cout<<"O";}
            
        }
    cout<<endl;     
     }
 };
void PrintMesh(int x,int y)
{
    int deltax=(x-_x);
    int deltay=(y-_y);
    //cout<<"deltax "<<deltax<<endl;
    
    //cout<<"deltay "<<deltay<<endl;
       
    if(abs(deltax)>=ceil(M/2))
    {
        
        if(deltax>0)
        {
            deltax=deltax-M;
        }
        else{deltax=deltax+M;}
    //cout<<"deltax"<<deltax<<endl;    
        
    }
    if(abs(deltay)>=ceil(N/2))
    {
        if(deltay>0){
            deltay=deltay-N;
        }
        else{deltay=deltax+N;}
    //cout<<"deltay "<<deltay<<endl;
    }
    
    
    
    if(deltax>0)
    {
        for(int i=1;i<=deltax;i++)
            cout<<"down ";
        cout<<endl;    
    }
    else
    {
        for(int i=deltax;i<0;i++)
            cout<<"up ";
        cout<<endl;    
    };
    if(deltay>0)
    {
        for(int i=1;i<=deltay;i++)
            cout<<"right ";
        cout<<endl;    
    }
    else
    {
        for(int i=deltay;i<0;i++)
            cout<<"left ";
        cout<<endl;    
    };    
} 
 
};



int main()
{
    //cout<<"Hello World";
    Robot r(1,1);
    r.PrintMesh();
    r.PrintMesh(3,4);
    return 0;
}

