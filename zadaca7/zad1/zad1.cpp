/******************************************************************************

                              Online C++ Compiler.
               Code, Compile, Run and Debug C++ program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <iostream>
#include<list>
#include<algorithm>
using namespace std;



template <typename K, typename V>
struct MinPriorityQueue {
 list<pair<K, V>> container;
 void insert(const pair<K, V>& el)
{
    container.push_back(el);
    container.sort();
};
 V extractMin()
 {
     auto i=(*container.begin()).second;
     container.erase(container.begin());
     return i;
 };
 // pronalazimo element s kljucem key, te ga postavljamo
 // na odgovarajuce mjesto s obzirom na njegov novi kljuc
 // newKey
 void decreaseKey(const K& key, const K& newKey)
 {
     for(auto i=container.begin();i!=container.end();i++)
     {
         if((*i).first==key)
         {(*i).first=newKey;}
     }
     container.sort();
 };
 //void print(){for(auto i=container.begin();i!=container.end();i++){cout<<(*i).first<<" "<<(*i).second<<endl;}}

};

template <typename K, typename V>
ostream& operator<<(ostream& os, const MinPriorityQueue<K,V>& q)
{
    for(auto i=q.container.begin();i!=q.container.end();i++){cout<<"K: "<<(*i).first<<", V: "<<(*i).second<<";  ";}
    cout<<endl;
    return os;
}



int main()
{

    MinPriorityQueue<int, string> q;
    q.insert(make_pair(1,"osoba4"));
    q.insert(make_pair(2,"osoba7"));
    q.insert(make_pair(5,"osoba6"));
    q.insert(make_pair(7,"osoba5"));
    q.insert(make_pair(8,"osoba1"));
    cout<<q;
    q.insert(make_pair(6,"osoba9"));
    cout<<q;
    q.extractMin();
    cout<<q;
    q.decreaseKey(6,4);
    //q.print();
    cout<<q;
    
    
    return 0;
}

