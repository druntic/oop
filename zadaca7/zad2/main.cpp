/******************************************************************************

                              Online C++ Compiler.
               Code, Compile, Run and Debug C++ program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include "cbt.h"

int main()
{
    vector<pair<int,int>> L;
    L.push_back(make_pair(0,2)); // first place is not used!!! Dummy push_back!
    L.push_back(make_pair(2,4));
    L.push_back(make_pair(2,5));
    L.push_back(make_pair(2,6));
    L.push_back(make_pair(2,7));
    CompleteBinaryTree<int,int> t(L);
    t.updateLeft(2, make_pair(1,1));
    //t.preorderPrint(1);
    cout<<t.empty();
    cout<<t[2].second;
    return 0;
}
