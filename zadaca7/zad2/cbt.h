//
// Created by David on 4/30/2019.
//

#ifndef UNTITLED5_CBT_H
#define UNTITLED5_CBT_H
#include <iostream>
#include <vector>

using namespace std;
typedef unsigned int Node;
template <typename K, typename V>
struct CompleteBinaryTree {

    vector<pair<K, V>> N;
    unsigned int size;
    CompleteBinaryTree(){size=1;};
    CompleteBinaryTree(const vector<pair<K, V>>& L)
    {
        N=L;
        size=L.size();
    };
    Node left(Node i) const{return 2*i;};
    Node right(Node i) const{return 2*i+1;};
    Node parent(Node i) const{return i/2;};
    pair<K, V>& element(Node i){return N[i];};
    const pair<K, V>& element(Node i) const{return N[i];};
    void updateLeft(Node i, const pair<K, V>& e){N[2*i]=e;};
    void updateRight(Node i, const pair<K, V>& e){N[2*i+1]=e;};
    void updateParent(Node i, const pair<K, V>& e){N[i/2]=e;};

    bool isLeaf(Node i) const{return left(i) > size;};
    unsigned int getSize(){return size;};
    void setSize(unsigned int newSize){size=newSize;};
    bool empty() const{return size==0;};


    void preorderPrint(Node i) const
    {
        if (i > size)
            return;
        cout <<  N[i].second << "  ";
        postorderPrint(left(i));
        postorderPrint(right(i));

    };
    void postorderPrint(Node i) const
    {

        if (i > size)
            return;
        postorderPrint(left(i));
        postorderPrint(right(i));
        cout <<  N[i].second << "  ";
    };
    void swapNodes(Node i, Node j){auto x=N[i];N[i]=N[j];N[j]=x;};
    const pair<K, V>& operator[](int i) const{return element(i);};
    pair<K, V>& operator[](int i){return element(i);};

};

#endif //UNTITLED5_CBT_H
