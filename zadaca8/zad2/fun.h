//
// Created by David on 5/14/2019.
//

#ifndef DZ8ZAD2_FUN_H
#define DZ8ZAD2_FUN_H
#include <iostream>
#include <vector >
using namespace std;


template <typename K, typename V>
struct Node {
    K key;
    V value;
    Node()= default;
    Node(const K& k,const  V& v){key=k;value=v;}
    Node* parent= nullptr, * left= nullptr, * right= nullptr;

    template<class T, class Y>
    friend ostream& operator<<(ostream& os, const Node<T,Y>& n);
    ~Node<K,V>()
    {
        delete left;
        delete right;

    }

};
template <typename K, typename V>
Node<K,V>* newNode(const K& key,const V& value)
{
    auto n=new Node<K,V>(key,value);
    n->right= nullptr;
    n->left= nullptr;

    return n;
};

template <typename K, typename V>
ostream& operator<<(ostream& os, const Node<K,V>& n)
{
    cout<<"Node at: "<<&n<<" parent at: "<<n.parent<<" left at: "<<n.left<<" right at: "<<n.right<<endl;
    cout<<"Key: "<<n.key<<" Value: "<<n.value<<endl;
    return os;
};



template <typename K, typename V>
struct BST {
    Node<K, V>* root= nullptr;

    BST()= default;
    // copy konstruktor (pripaziti na shallow-copy)
    BST(const BST& bst)
    {
        root= new Node<K,V>(bst.root->key,bst.root->value);
        copy(root, bst.root);
    };
    ~BST(){vector<Node<K,V>*> toDelete;
        inorderWalk(root, toDelete);
        for(auto i:toDelete)
            delete i;
    };
    // pretrazuj podstablo s korijenom x dok ne pronadeš čvor
    // vrijednoscu key (u suprotnom vrati nullptr)
    void copy(Node<K,V>* a, Node<K,V>* b)//u a kopiramo, iz b kopiramo
    {
        if(b->left!= nullptr)
        {
            //cout<<"do it left"<<endl;
            a->left=new Node<K,V>(b->left->key,b->left->value);
            //insert(a->left, b->left->key, b->left->value);
            a->left->parent=a;
            copy(a->left, b->left);
        }
        if(b->right!= nullptr)
        {
            //cout<<"do it right"<<endl;
            a->right=new Node<K,V>(b->right->key,b->right->value);
            //insert(a->right, b->right->key, b->right->value);
            a->right->parent=a;
            copy(a->right, b->right);
        }

    }

    Node<K, V> *search(Node<K, V> *root, K key) {
        if (root == nullptr || root->key == key)
            return root;

        if (root->key < key)
            return search(root->right, key);

        return search(root->left, key);
    }

    // vrati pokazivač na čvor koji ima minimalnu vrijednost
    // kljuca u podstablu čiji je korijen x
    Node<K, V>* minimum(Node<K, V>* x)
    {
        if(x->left!= nullptr)
            minimum(x->left);
        else{cout<<x->value<<endl;
            return x;}
    };
    // vrati pokazivač na čvor koji ima maksimalnu vrijednost
    // kljuca u podstablu čiji je korijen x
    Node<K, V>* maximum(Node<K, V>* x)
    {
        if(x->right!= nullptr)
            minimum(x->right);
        else{cout<<x->value<<endl;
            return x;}
    };
    // vrati sljedbenika čvora x po vrijednosti key unutar stabla
    Node<K, V>* successor(Node<K, V>* x)
    {
        x=x->right;
        while(x->left!= nullptr){x=x->left;};
        //cout<<*x;
        return x;
    };
    // vrati prethodnika čvora x po vrijednosti key unutar stabla
    Node<K, V>* predecessor(Node<K, V>* x)
    {
        x=x->left;
        while(x->right!= nullptr){x=x->right;};
        cout<<*x;
    };



    Node<K,V>* insert(Node<K,V>* node,const K& key, const V& value)
    {

        if(root== nullptr){root= newNode(key,value);root->parent= nullptr;}
        if (node == nullptr){node= newNode(key,value);}
        if (key < node->key)
        {node->left=insert(node->left, key,value);node->left->parent=node;}
        else if (key > node->key)
        {node->right=insert(node->right, key,value);node->right->parent=node;}
        return node;
    };
    void inorder(Node<K,V>* &n)
    {
        if (n != nullptr)
        {
            if(n->parent== nullptr){
                cout<<"Root at: "<<&(*n)<<endl;
            }
            inorder(n->left);
            cout<<*n<<endl;
            inorder(n->right);
        }
    };
    void inorderWalk(Node<K, V>* x, vector<Node<K, V>*>& nodes) const
    {
        if (x != nullptr)
        {
            inorderWalk(x->left,nodes);
            nodes.push_back(x);
            //cout<<*x<<endl;
            inorderWalk(x->right,nodes);
        }
    };

    void transplantNodes(Node<K, V>* u, Node<K, V>* v)
    {
        auto key=u->key;
        auto value=u->value;
        u->key=v->key;
        u->value=v->value;
        v->value=value;
        v->key=key;
    };
    void transplant(Node<K, V>* u, Node<K, V>* v)
    {
        auto x=u;
        if(u->parent->left==u)
        {
            u->parent->left=v;
        }
        else if(u->parent->right==u)
        {
            u->parent->right=v;
        }
        if(v->parent->left==v)
        {
            v->parent->left=x;
        }
        else if(v->parent->right==v)
        {
            v->parent->right=x;
        }
    }
    // obriši čvor x brinuvši se o definiciji binary search tree-a
    void remove(Node<K, V>* x)
    {
        if(x->left==nullptr and x->right== nullptr)//leaf
        {
            if(x->parent->left==x){delete x->parent->left;x->parent->left= nullptr;}
            if(x->parent->right==x){delete x->parent->right;x->parent->right= nullptr;}

        }
        if(x->left!= nullptr and x->right== nullptr)
        {
            transplantNodes(x,x->left);
            remove(x->left);
        }
        if(x->left== nullptr and x->right!= nullptr)
        {
            transplantNodes(x,x->right);
            remove(x->right);
        }
        else if(x->right!= nullptr and x->left!= nullptr)
        {
            transplantNodes(x,successor(x));
            remove(successor(x));
        }

    };
    // copy pridruživanje (pripaziti na shallow-copy)
    BST& operator=(const BST& bst)
    {
        root= new Node<K,V>(bst.root->key,bst.root->value);
        copy(root, bst.root);
        return *this;
    };
    template <class A, class B>
    friend ostream& operator<<(ostream& os, BST<A,B>& bst);


};
template <typename K, typename V>
ostream& operator<<(ostream& os, BST<K,V>& bst){bst.inorder(bst.root);return os;}

#endif //DZ8ZAD2_FUN_H
