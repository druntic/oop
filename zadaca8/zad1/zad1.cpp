/******************************************************************************

                              Online C++ Compiler.
               Code, Compile, Run and Debug C++ program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <iostream>
#include <regex>
using namespace std;

class LoginCredentials {
private:
 string confirmPassword;
protected:
 string email, password;
public:
 LoginCredentials();
 /*prosljedujemo email, password i confirm password*/
 LoginCredentials(const string& e, const string& p, const string& cp){email=e;password=p;confirmPassword=cp;};
 bool validEmail() const
 {
     regex r("([a-z]+)\\@");
     smatch m;
     regex_search(email, m, r);
    if(m.empty()==true){return false;}
    return true;
 };
 friend ostream& operator<<(ostream& buffer, const LoginCredentials&);
  friend class Account;
};

ostream& operator<<(ostream& buffer, const LoginCredentials& x)
{
    cout<<"Email: "<<x.email<<" Password: "<<x.password<<" Confirm password: "<<x.confirmPassword<<endl;
    return buffer;
};


class Account {
protected:
 string username;
public:
 Account();
 /*prosljedujemo username, email i password*/
 Account(const string& u, const string& e, const string& p){username=u;email=e;password=p;};
 string getUsername(){return username;};
 string getEmail(){return email;};
 friend ostream& operator<<(ostream& buffer, const Account&);
 friend class LoginCredentials;
};

int main()
{
    LoginCredentials d("osdavidruntic@gmail.com", "123", "12");
    cout<<d.validEmail();
    cout<<d;
    return 0;
}

