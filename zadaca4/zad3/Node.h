//
// Created by David on 3/24/2019.
//

#ifndef DZ4ZAD3_NODE_H
#define DZ4ZAD3_NODE_H
struct Node{
    double value;
    Node* next;

    Node();
    Node(double value);
    Node(const Node& n);
    ~Node();

    void print() const;
};



#endif //DZ4ZAD3_NODE_H
