//
// Created by David on 3/24/2019.
//

#ifndef DZ4ZAD3_CSLL_H
#define DZ4ZAD3_CSLL_H

#include "Node.h"
class CSLL{
protected:
    Node* tail;
public:
    CSLL();
    CSLL(const CSLL& c);
    ~CSLL();

    bool empty() const;

    void prepend(double value);
    void append(double value);

    double removeFromHead();

    void print() const;
};

#endif //DZ4ZAD3_CSLL_H
