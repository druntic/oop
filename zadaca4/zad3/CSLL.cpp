//
// Created by David on 3/24/2019.
//

#include "CSLL.h"
#include <iostream>
#include "Node.h"
using namespace std;

CSLL::CSLL(): tail(nullptr) {}
CSLL::CSLL(const CSLL &c)
{
   /* Node* it=c.tail->next;
    while(it!=c.tail)
    {
        append(it->value);
        it=it->next;
    }
    append(it->value);*/
    tail->next= nullptr;
    *this=c;
};
CSLL::~CSLL()
{
    //cout<<"destroy!"<<endl;
    Node* temp=tail->next;
    while(temp!=tail)
    {
        Node* p=temp;
        temp=temp->next;
        //tail->next=head;
        delete p;
    }
    delete temp;
}

bool CSLL::empty() const {
    return tail == nullptr;
}

void CSLL::prepend(double value) {
    Node * n = new Node(value);
    if(empty()){
        tail = n;
        n->next=tail;
    }else{
        n->next=tail->next;
        tail->next=n;


    }
}

void CSLL::append(double value) {
    Node * n = new Node(value);
    if(empty()){
        tail = n;
        n->next=tail;
    }else
        {
            n->next=tail->next;
            tail->next=n;
            tail=n;
        }
}

void CSLL::print() const {
    cout << "tail at: " << tail << endl;

    if(!empty()){

        Node * it = tail->next;
        while(it!=tail){

            it->print();
            it = it->next;
        };
        it->print();

    }

}