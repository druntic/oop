//
// Created by David on 3/24/2019.
//

#include <iostream>
#include <utility>
#include <algorithm>
#include "Node.h"
#include "CDLL.h"

using namespace std;



Node* head;
Node* tail;

CDLL::CDLL(){head=nullptr; tail=nullptr;};
CDLL::CDLL(const CDLL& c)
{
    head= nullptr;
    tail= nullptr;
    CDLL l;
    Node* temp=c.head;
    while(temp!=c.tail)
    {

        if(head==nullptr and tail==nullptr)
        {
            Node *n= new Node(temp->value);
            n->next=head;
            n->prev=tail;
            head=n;
            tail=n;

        }
        else
        {
            Node *n=new Node(temp->value);
            head->prev=n;
            tail->next=n;
            n->prev=tail;
            n->next=head;
            tail=n;


        }



        temp=temp->next;
    }

    if(head==nullptr and tail==nullptr)
    {
        Node *n= new Node(temp->value);
        n->next=head;
        n->prev=tail;
        head=n;
        tail=n;

    }
    else
    {
        Node *n=new Node(temp->value);
        head->prev=n;
        tail->next=n;
        n->prev=tail;
        n->next=head;
        tail=n;


    }
};
CDLL::~CDLL()
{
    Node* temp=head;
    while(temp!=tail)
    {
        temp=temp->next;
        Node* p=head;
        head=head->next;
        tail->next=head;
        delete p;
    }
    delete head;
};
bool CDLL::empty() const
{
    if(head==tail){return true;}
    else{return false;}
};
// postavi vrijednost na početak
void CDLL::prepend(double value)
{
    if(head==nullptr and tail==nullptr)
    {
        Node *n= new Node(value);
        n->next=head;
        n->prev=tail;
        head=n;
        tail=n;

    }
    else
    {
        Node *n=new Node(value);
        head->prev=n;
        tail->next=n;
        n->prev=tail;
        n->next=head;
        head=n;

    }
};
// postavi vrijednost na kraj
void CDLL::append(double value)
{    if(head==nullptr and tail==nullptr)
    {
        Node *n= new Node(value);
        n->next=head;
        n->prev=tail;
        head=n;
        tail=n;

    }
    else
    {
        Node *n=new Node(value);
        head->prev=n;
        tail->next=n;
        n->prev=tail;
        n->next=head;
        tail=n;
    };
};
// ukloni čvor s početka
// i vrati njegovu vrijednost
double CDLL::removeFromHead()
{
    double v=head->value;
    Node* p=head;
    head=head->next;
    tail->next=head;
    delete p;
    return v;
};
// ukloni čvor s kraja
// i vrati njegovu vrijednost
double CDLL::removeFromTail()
{
    double v=tail->value;
    Node* p=tail;
    tail=tail->prev;
    head->prev=tail;
    delete p;
    return v;

};
// ispisujemo adresu head-a, tail-a te sve čvorove
void CDLL::print() const
{
    cout<<"Head at: "<<head<<", Tail at: "<<tail<<endl;
    Node* temp=head;
    while(temp!=tail)
    {
        //cout<<"prev at: "<<temp->prev<<", "<<"current at: "<<temp<<", next at: "<<temp->next<<", "<<"value: "<<temp->value<<endl;
        temp->print();
        temp=temp->next;
    }
    temp->print();
    //cout<<"prev at: "<<temp->prev<<", "<<"current at: "<<temp<<", next at: "<<temp->next<<", "<<"value: "<<temp->value<<endl;
};
void CDLL::sort()
{
    Node* temp=head;
    int i=0;
    while(temp!=tail)
    {
        i++;
        temp=temp->next;
    }
    i++;

    for(int j=0;j<i;j++)
    {Node* temp1=head;
        while (temp1!=tail)
        {
            Node* temp2=temp1->next;
            if((temp1->value)<(temp2->value))
            {
                double v1=temp1->value;
                temp1->value=temp2->value;
                temp2->value=v1;
            }
            temp1=temp1->next;
        }
    }

};