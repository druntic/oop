//
// Created by David on 3/24/2019.
//
#pragma once
#ifndef DZ4ZAD2_NODE_H
#define DZ4ZAD2_NODE_H

struct Node {
    Node* prev;
    Node* next;
    double value;
    Node();


    Node(double value);
    Node(const Node& n);
    // prilikom čišćenja ispisujemo adresu
    ~Node();

    //swap premješta sadržaj između dva čvora
    void swap(Node& n);
    // ispisujemo adresu, adresu prethodnog, adresu sljedećeg te vrijednost
    void print() const;
};



#endif //DZ4ZAD2_NODE_H
