//
// Created by David on 3/24/2019.
//
#pragma once
#ifndef DZ4ZAD2_CDLL_H
#define DZ4ZAD2_CDLL_H

#include "Node.h"

class CDLL {
protected:
    Node* head;
    Node* tail;
public:
    CDLL();
    CDLL(const CDLL& c);
    ~CDLL();
    bool empty() const;
    // postavi vrijednost na početak
    void prepend(double value);
    // postavi vrijednost na kraj
    void append(double value);
    // ukloni čvor s početka
    // i vrati njegovu vrijednost
    double removeFromHead();
    // ukloni čvor s kraja
    // i vrati njegovu vrijednost
    double removeFromTail();
    // ispisujemo adresu head-a, tail-a te sve čvorove
    void print() const;
    void sort();
};

#endif //DZ4ZAD2_CDLL_H
