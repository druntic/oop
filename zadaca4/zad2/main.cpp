/******************************************************************************

                              Online C++ Compiler.
               Code, Compile, Run and Debug C++ program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <iostream>
#include <utility>
#include <algorithm>
#include "CDLL.h"
#include "Node.h"

using namespace std;


int main()
{


    CDLL* l1=new CDLL();
    l1->append(59.9);
    l1->append(13.7);
    l1->append(10.0);
    l1->append(98.44);
    l1->append(16.7);
    l1->append(20.269);
    l1->append(1.5);
    l1->print();

    CDLL* l2=new CDLL(*l1);
    l2->sort();
    l2->print();
    delete l1;
    delete l2;

    return 0;
}


