//
// Created by David on 3/24/2019.
//


#include <iostream>
#include <utility>
#include <algorithm>
#include "Node.h"
using namespace std;


    Node* prev;
    Node* next;
    double value;
    Node::Node(){};
    Node::Node(double n)
    {
        value=n;
        prev=nullptr;
        next=nullptr;
    };
    Node::Node(const Node& n)
    {
        value=n.value;
        next=n.next;
        prev=n.prev;
    };
    // prilikom čišćenja ispisujemo adresu
    Node::~Node()
    {
        cout<<"Delete node at: "<<this<<endl;
        prev= nullptr;
        next= nullptr;        
    };

    //swap premješta sadržaj između dva čvora
    void Node::swap(Node& n)
    {
        double x=n.value;
        n.value=value;
        value=x;


    };
    // ispisujemo adresu, adresu prethodnog, adresu sljedećeg te vrijednost
    void Node::print() const
    {
        cout<<"Node at: "<<this<<", "<<"prev at: "<<this->prev<<", "<<"next at: "<<this->next<<", "<<"value: "<<value<<endl;
    };
