//
// Created by David on 3/25/2019.
//

#ifndef TESST_CSLL_H
#define TESST_CSLL_H
#include "Node.h"
class CSLL{
protected:
    Node* tail;
public:
    CSLL();
    CSLL(const CSLL& c);
    ~CSLL();

    bool empty() const;

    void prepend(double value);
    void append(double value);

    double removeFromHead();

    void print() const;
};

#endif //TESST_CSLL_H
