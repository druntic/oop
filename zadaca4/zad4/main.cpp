#include <iostream>
#include "CSLL.h"
#include "DynamicQueue.h"
using namespace std;

int main(){

DynamicQueue* q= new DynamicQueue();
    q->enqueue(59.9);
    q->enqueue(13.7);
    q->enqueue(10.0);
    q->enqueue(98.44);
    q->enqueue(16.7);
    q->enqueue(20.269);
    q->enqueue(1.5);
    q->print();

    DynamicQueue* q2=new DynamicQueue(*q);
    q2->print();
    delete q;
    delete q2;

    return 0;
}