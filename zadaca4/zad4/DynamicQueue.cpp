//
// Created by David on 3/25/2019.
//

#include "DynamicQueue.h"
#include <iostream>

using namespace std;
DynamicQueue::DynamicQueue(){};

DynamicQueue::DynamicQueue(const DynamicQueue& q){*this=q;};

DynamicQueue::~DynamicQueue(){};

bool DynamicQueue::empty() const{return container.empty();};

void DynamicQueue::enqueue(double x){container.append(x);};

double DynamicQueue::dequeue(){container.removeFromHead();};

void DynamicQueue::print() const{container.print();};

