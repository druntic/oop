//
// Created by David on 3/25/2019.
//

#ifndef TESST_NODE_H
#define TESST_NODE_H
struct Node{
    double value;
    Node* next;

    Node();
    Node(double value);
    Node(const Node& n);
    ~Node();

    void print() const;
};


#endif //TESST_NODE_H
