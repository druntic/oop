//
// Created by David on 3/25/2019.
//

#ifndef TESST_DYNAMICQUEUE_H
#define TESST_DYNAMICQUEUE_H

#include "CSLL.h"
class DynamicQueue {
protected:
 CSLL container;
public:
 DynamicQueue();


 DynamicQueue(const DynamicQueue& q);

 ~DynamicQueue();

 bool empty() const;

 void enqueue(double x);

 double dequeue();

 void print() const;
};

#endif //TESST_DYNAMICQUEUE_H
