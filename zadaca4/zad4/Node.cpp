//
// Created by David on 3/25/2019.
//


#include "Node.h"
#include <iostream>

using namespace std;

Node::Node(): value(0), next(nullptr) {}
Node::Node(double value) : value(value), next(nullptr) {}
Node::Node(const Node &n) : value(n.value), next(n.next) {}
Node::~Node()
{
    cout<<"Delete node at: "<<this<<endl;
    next=nullptr;
};

void Node::print() const {
    cout << "Node at: " << this << "; next at: " << this->next << "; value: " << value<<endl;
}