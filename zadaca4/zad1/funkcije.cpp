//
// Created by David on 3/23/2019.
//

#include "funkcije.h"
#include <iostream>
#include<list>
#include<algorithm>
#include "funkcije.h"

using namespace std;

double sum(list<double>& fifth)
{
    double sum=0;
    for (list<double>::iterator it = fifth.begin(); it != fifth.end(); it++)
    {sum=sum+*it;}
    return sum;
}
double prod(list<double>& fifth)
{
    double prod=1;
    for (list<double>::iterator it = fifth.begin(); it != fifth.end(); it++)
    {prod=prod*(*it);}
    return prod;
}

double min(list<double>& fifth)
{
    list<double>::iterator iter = min_element(fifth.begin(), fifth.end());
    return *iter;
}
double max(list<double>& fifth)
{
    list<double>::iterator iter = max_element(fifth.begin(), fifth.end());
    return *iter;
}